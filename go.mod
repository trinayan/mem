module gitlab.com/akita/mem/v2

require (
	github.com/golang/mock v1.4.4
	github.com/google/btree v1.0.0
	github.com/onsi/ginkgo v1.15.1
	github.com/onsi/gomega v1.10.5
	github.com/rs/xid v1.2.1
	gitlab.com/akita/akita/v2 v2.0.0-alpha.3
	gitlab.com/akita/util/v2 v2.0.0-alpha.3
	golang.org/x/sys v0.0.0-20210313110737-8e9fff1a3a18 // indirect
)

// replace gitlab.com/akita/akita => ../akita

// replace gitlab.com/akita/util => ../util

go 1.16
