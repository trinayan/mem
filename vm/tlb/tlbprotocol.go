package tlb

import (
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/util/v2/ca"
)

// A TLBFlushReq asks the TLB to invalidate certain entries. It will also not block all incoming and outgoing ports
type TLBFlushReq struct {
	sim.MsgMeta
	VAddr []uint64
	PID   ca.PID
}

// Meta returns the meta data associated with the message.
func (r *TLBFlushReq) Meta() *sim.MsgMeta {
	return &r.MsgMeta
}

// TLBFlushReqBuilder can build AT flush requests
type TLBFlushReqBuilder struct {
	sendTime sim.VTimeInSec
	src, dst sim.Port
	vAddrs   []uint64
	pid      ca.PID
}

// WithSendTime sets the send time of the request to build.:w
func (b TLBFlushReqBuilder) WithSendTime(
	t sim.VTimeInSec,
) TLBFlushReqBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b TLBFlushReqBuilder) WithSrc(src sim.Port) TLBFlushReqBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b TLBFlushReqBuilder) WithDst(dst sim.Port) TLBFlushReqBuilder {
	b.dst = dst
	return b
}

// WithVAddrs sets the Vaddr of the pages to be flushed
func (b TLBFlushReqBuilder) WithVAddrs(vAddrs []uint64) TLBFlushReqBuilder {
	b.vAddrs = vAddrs
	return b
}

// WithPID sets the pid whose entries are to be flushed
func (b TLBFlushReqBuilder) WithPID(pid ca.PID) TLBFlushReqBuilder {
	b.pid = pid
	return b
}

// Build creates a new TLBFlushReq
func (b TLBFlushReqBuilder) Build() *TLBFlushReq {
	r := &TLBFlushReq{}
	r.ID = sim.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime
	r.VAddr = b.vAddrs
	r.PID = b.pid
	return r
}

// A TLBFlushRsp is a response from AT indicating flush is complete
type TLBFlushRsp struct {
	sim.MsgMeta
}

// Meta returns the meta data associated with the message.
func (r *TLBFlushRsp) Meta() *sim.MsgMeta {
	return &r.MsgMeta
}

// TLBFlushRspBuilder can build AT flush rsp
type TLBFlushRspBuilder struct {
	sendTime sim.VTimeInSec
	src, dst sim.Port
}

// WithSendTime sets the send time of the request to build.:w
func (b TLBFlushRspBuilder) WithSendTime(
	t sim.VTimeInSec,
) TLBFlushRspBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b TLBFlushRspBuilder) WithSrc(src sim.Port) TLBFlushRspBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b TLBFlushRspBuilder) WithDst(dst sim.Port) TLBFlushRspBuilder {
	b.dst = dst
	return b
}

// Build creates a new TLBFlushRsps.
func (b TLBFlushRspBuilder) Build() *TLBFlushRsp {
	r := &TLBFlushRsp{}
	r.ID = sim.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime

	return r
}

// A TLBRestartReq is a request to TLB to start accepting requests and resume operations
type TLBRestartReq struct {
	sim.MsgMeta
}

// Meta returns the meta data associated with the message.
func (r *TLBRestartReq) Meta() *sim.MsgMeta {
	return &r.MsgMeta
}

// TLBRestartReqBuilder can build TLB restart requests.
type TLBRestartReqBuilder struct {
	sendTime sim.VTimeInSec
	src, dst sim.Port
}

// WithSendTime sets the send time of the request to build.
func (b TLBRestartReqBuilder) WithSendTime(
	t sim.VTimeInSec,
) TLBRestartReqBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b TLBRestartReqBuilder) WithSrc(src sim.Port) TLBRestartReqBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b TLBRestartReqBuilder) WithDst(dst sim.Port) TLBRestartReqBuilder {
	b.dst = dst
	return b
}

// Build creates a new TLBRestartReq.
func (b TLBRestartReqBuilder) Build() *TLBRestartReq {
	r := &TLBRestartReq{}
	r.ID = sim.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime

	return r
}

// A TLBRestartRsp is a response from AT indicating it has resumed working
type TLBRestartRsp struct {
	sim.MsgMeta
}

// Meta returns the meta data associated with the message.
func (r *TLBRestartRsp) Meta() *sim.MsgMeta {
	return &r.MsgMeta
}

// TLBRestartRspBuilder can build AT flush rsp
type TLBRestartRspBuilder struct {
	sendTime sim.VTimeInSec
	src, dst sim.Port
}

// WithSendTime sets the send time of the request to build.:w
func (b TLBRestartRspBuilder) WithSendTime(
	t sim.VTimeInSec,
) TLBRestartRspBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b TLBRestartRspBuilder) WithSrc(src sim.Port) TLBRestartRspBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b TLBRestartRspBuilder) WithDst(dst sim.Port) TLBRestartRspBuilder {
	b.dst = dst
	return b
}

// Build creates a new TLBRestartRsp
func (b TLBRestartRspBuilder) Build() *TLBRestartRsp {
	r := &TLBRestartRsp{}
	r.ID = sim.GetIDGenerator().Generate()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime

	return r
}
